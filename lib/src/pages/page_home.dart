import 'package:flutter/material.dart';
import 'package:peliculas/src/providers/peliculas.provider.dart';
import 'package:peliculas/src/widgets/anothercharge.dart';
import 'package:peliculas/src/widgets/card_swiper_widget.dart';

class HomePage extends StatelessWidget {
  final peliculasProvider = new PeliculasProvider();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Peliculas en Cines'),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.search), onPressed: null)
          ],
        ),
        //body: SafeArea(child: Text('Hola Mundo')));
        body: Container(
          child: Column(
            children: <Widget>[
              Container(child:_swipperTarjetas(), height: 350.0,),
              Text('Otra lista de peliculas'),
              Container(child: _bottomswiper(), height: 150.0,),
            ],
        )));
  }

  Widget _swipperTarjetas() {

    return FutureBuilder(
      future:  peliculasProvider.getEnCines(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if(snapshot.hasData){
          return CardSwiper(peliculas: snapshot.data);
        }
        else{
          return Container(
            height: 350.0,
            child: Center(
              child: CircularProgressIndicator()
            )
          ); 
        }
      },
    );
  }

  Widget _bottomswiper() {
    return FutureBuilder(
      future:  peliculasProvider.getEnCines(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if(snapshot.hasData){
          return BottomSwiper(peliculas: snapshot.data);
        }
        else{
          return Container(
            child: Center(
              child: CircularProgressIndicator()
            )
          ); 
        }
      },
    );
  }
}
