import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:peliculas/src/models/pelicula_model.dart';

class BottomSwiper extends StatelessWidget {
  final List<Pelicula> peliculas;
  BottomSwiper({@required this.peliculas});

  @override
  Widget build(BuildContext context) {

    return Container(
      padding: EdgeInsets.only(top: 10.0),
      child: Swiper(
        itemWidth: 115.0,
        itemHeight: 140.0,
        layout: SwiperLayout.TINDER,

        itemBuilder: (BuildContext context, int index) {
          return ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              /*  child: Image.network(
                "http://via.placeholder.com/350x150",
                fit: BoxFit.cover,
              ) */
              child: FadeInImage(
                image: NetworkImage(peliculas[index].getPosterImg()),
                placeholder: AssetImage('assets/loading.gif'),
                fit: BoxFit.fitHeight,
              ));
        },
        itemCount: peliculas.length,
      ),
    );
  }
}
