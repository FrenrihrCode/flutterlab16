import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:peliculas/src/models/pelicula_model.dart';

class PeliculasProvider {
  //api key de la api y demas configuraciones
  String _apikey = '7814cnn34ae79e8941c602b9ce281d18';
  String _url = 'api.themoviedb.org';
  String _language = 'es-ES';
  Future <List<Pelicula>> getEnCines() async {

  final url = Uri.https(_url, '3/movie/now_playing', {
    'api_key': _apikey,
    'language': _language
  });
  final resp = await http.get(url); //se usa await similar a js
  final decodedData = json.decode(resp.body);
  final peliculas = new Peliculas.fromJsonList(decodedData['results']);
  //decodificamos el json y lo codificamos de nuevo bajo la clase pelucula que seria el modelo
    return peliculas.items;
  }
}
